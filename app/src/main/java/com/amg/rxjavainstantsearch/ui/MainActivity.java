package com.amg.rxjavainstantsearch.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.amg.rxjavainstantsearch.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        whiteNotificationBar(binding.toolbar);

        binding.contentMain.btnLocalSearch.setOnClickListener(
                view -> intent2Activity(LocalSearchActivity.class)
        );

        binding.contentMain.btnRemoteSearch.setOnClickListener(
                view -> intent2Activity(RemoteSearchActivity.class));

    }

    private void intent2Activity(Class<?> cls) {
        startActivity(new Intent(MainActivity.this, cls));
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

}