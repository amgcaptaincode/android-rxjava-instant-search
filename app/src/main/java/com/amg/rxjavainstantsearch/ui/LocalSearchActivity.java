package com.amg.rxjavainstantsearch.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.amg.rxjavainstantsearch.databinding.ActivityLocalSearchBinding;
import com.amg.rxjavainstantsearch.interfaces.ILocalSearchView;
import com.amg.rxjavainstantsearch.network.model.Contact;
import com.amg.rxjavainstantsearch.presenter.LocalSearchPresenter;
import com.amg.rxjavainstantsearch.ui.adapters.ContactsAdapterFilterable;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class LocalSearchActivity extends AppCompatActivity
        implements ContactsAdapterFilterable.ContactsAdapterListener, ILocalSearchView {

    private ActivityLocalSearchBinding binding;
    private ContactsAdapterFilterable mAdapter;
    private List<Contact> contactsList = new ArrayList<>();

    private LocalSearchPresenter presenter;

    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLocalSearchBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        presenter = new LocalSearchPresenter(this);

        setSupportActionBar(binding.toolbarLocalSearch);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mAdapter = new ContactsAdapterFilterable(this, contactsList, this);

        setupRecyclerView();

        whiteNotificationBar(binding.contentLocalSearch.recyclerView);

        setupListeners();

        presenter.fetchContacts("gmail");
    }

    private void setupListeners() {

        disposable.add(
                RxTextView.textChangeEvents(binding.contentLocalSearch.inputSearch)
                        .skipInitialValue()
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .distinctUntilChanged()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(searchContacts())
        );
    }

    private DisposableObserver<TextViewTextChangeEvent> searchContacts() {
        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(@NonNull TextViewTextChangeEvent textViewTextChangeEvent) {
                mAdapter.getFilter().filter(textViewTextChangeEvent.text());
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    private void setupRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.contentLocalSearch.recyclerView.setLayoutManager(mLayoutManager);
        binding.contentLocalSearch.recyclerView.setItemAnimator(new DefaultItemAnimator());
        binding.contentLocalSearch.recyclerView.addItemDecoration(
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.contentLocalSearch.recyclerView.setAdapter(mAdapter);
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    @Override
    public void onContactSelected(Contact contact) {
    }

    @Override
    public void onShowContacts(List<Contact> contacts) {
        contactsList.clear();
        contactsList.addAll(contacts);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(@androidx.annotation.NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        disposable.clear();
        presenter.onDestroy();
        super.onDestroy();
    }
}