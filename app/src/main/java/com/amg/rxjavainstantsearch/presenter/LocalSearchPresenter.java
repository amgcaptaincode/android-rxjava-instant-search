package com.amg.rxjavainstantsearch.presenter;

import com.amg.rxjavainstantsearch.interfaces.ILocalSearchPresenter;
import com.amg.rxjavainstantsearch.interfaces.ILocalSearchView;
import com.amg.rxjavainstantsearch.network.ApiClient;
import com.amg.rxjavainstantsearch.network.ApiService;
import com.amg.rxjavainstantsearch.network.model.Contact;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LocalSearchPresenter implements ILocalSearchPresenter {

    private ILocalSearchView view;

    private CompositeDisposable disposable = new CompositeDisposable();
    private ApiService apiService;

    public LocalSearchPresenter(ILocalSearchView view) {
        this.view = view;

        apiService = ApiClient.getClient().create(ApiService.class);

    }

    @Override
    public void fetchContacts(String source) {
        disposable.add(apiService
                .getContacts(source, null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Contact>>() {
                    @Override
                    public void onSuccess(@NonNull List<Contact> contacts) {
                        view.onShowContacts(contacts);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                })
        );
    }

    @Override
    public void onDestroy() {
        disposable.clear();
    }
}
