package com.amg.rxjavainstantsearch.interfaces;

import com.amg.rxjavainstantsearch.network.model.Contact;

import java.util.List;

public interface ILocalSearchView {

    void onShowContacts(List<Contact> contacts);

}
