package com.amg.rxjavainstantsearch.interfaces;

public interface ILocalSearchPresenter {

    void fetchContacts(String source);

    void onDestroy();

}
